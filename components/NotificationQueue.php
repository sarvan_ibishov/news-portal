<?php
/**
 * Created by PhpStorm.
 * User: Sarvan Ibishov
 * Date: 11/20/2016
 * Time: 12:38 PM
 */

namespace app\components;

use app\models\User;
use  app\components\Notification;
use Yii;

class NotificationQueue
{

    //add notification queue
    public static function addNotificationQueue($post_id, $is_admin = false)
    {
        $query = User::find()->where('`role` <> 0 and (notification_type="browser" || notification_type="both")');
        if ($is_admin) {
            $query->Where('role=30');

        }
        $users = $query->all();

        foreach ($users as $user) {

            if ($is_admin) {
                Notification::notify(Notification::KEY_NEW_USER, $user->id, $post_id);
            } else {
                Notification::notify(Notification::KEY_NEW_MESSAGE, $user->id, $post_id);
            }
        }
    }

    //add mail queue
    public static function addEmailQueue($body, $is_admin = false)
    {

        $query = User::find()->where('`role` <> 0 and (notification_type="email" || notification_type="both")');
        if ($is_admin) {
            $query->Where('role=30');

        }
        $users = $query->all();

        foreach ($users as $user) {

            if ($is_admin) {

                self::addQueueTable('new_user', $body, Yii::$app->params['adminEmail'], $user->email, Yii::$app->params['subject2']);
            } else {

                self::addQueueTable('test', $body, Yii::$app->params['adminEmail'], $user->email, Yii::$app->params['subject']);
            }
        }
    }

    public static function addQueue($post_id, $body, $is_admin = false)
    {

        self::addNotificationQueue($post_id, $is_admin);
        self::addEmailQueue($body, $is_admin);
    }

    protected function addQueueTable($view, $textbody, $from, $to, $subject)
    {

        Yii::$app->mailqueue->compose($view, ['body' => $textbody])
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->queue();
    }

}