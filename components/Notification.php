<?php
/**
 * Created by PhpStorm.
 * User: Sarvan Ibishov
 * Date: 11/17/2016
 * Time: 11:06 PM
 */

namespace app\components;

use app\models\Post;
use app\models\User;
use Yii;

use machour\yii2\notifications\models\Notification as BaseNotification;

class Notification extends BaseNotification
{

    /**
     * A new message notification
     */
    const KEY_NEW_MESSAGE = 'new_message';
    const KEY_NEW_USER = 'new_user';
    /**
     * A meeting reminder notification
     */


    /**
     * @var array Holds all usable notifications
     */
    public static $keys = [
        self::KEY_NEW_MESSAGE,
        self::KEY_NEW_USER,

    ];

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        switch ($this->key) {


            case self::KEY_NEW_MESSAGE:
                return Yii::t('app', 'New article added');
            case self::KEY_NEW_USER:
                return Yii::t('app', 'New user registered');


        }
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {

        switch ($this->key) {


            case self::KEY_NEW_MESSAGE:
                $message = Post::findOne($this->key_id);
                if ($message !== null):
                    return Yii::t('app', '{customer} added', [
                        'customer' => $message->title
                    ]);
                endif;
            case self::KEY_NEW_USER:
                // echo self::KEY_NEW_USER;exit;
                $message = User::findOne($this->key_id);
                if ($message !== null):
                    return Yii::t('app', '{customer} registered', [
                        'customer' => $message->username
                    ]);
                endif;

        }
    }

    /**
     * @inheritdoc
     */
    public function getRoute()
    {
        switch ($this->key) {

            case self::KEY_NEW_MESSAGE:
                return ['/post/view', 'id' => $this->key_id];
            case self::KEY_NEW_USER:
                return ['/user/admin/update', 'id' => $this->key_id];

        };
    }

}