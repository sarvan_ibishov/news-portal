News portal
============================

Clone project first.

1. composer update
2. yii migrate

Description
-------------------

Project contains simple news website via access control. There 4 type of users in website.

Guest - only can view preview of news in homepage. role = 0

Authorized user - can view the all content of any news; role = 10

Moderator - can add, update, delete new post. role = 20

Administrator - Moderator + can manage of users; role = 30


First register in the website. Then give role = 30 for administrator user.
User can change his/her credentials in Settings menu. In Notification menu user can manage notification type.
There 3 type of notification. Email, browser and both of.

While moderator or administrator  adding new post, this post id inserting notification table and mail_queue table.
And users get notifications according to their notification type.
Email notifications managed from mail_queue table. To get mail notifications about news of website add cron this script.

~~~
* * * * * php /var/www/html/myapp/yii mail/send
~~~


Date: 14.11.2016 - 20.11.2016

Resume:

https://www.linkedin.com/in/sarvan-ibishov-47536a6a