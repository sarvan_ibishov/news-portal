<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">


    <? if (Yii::$app->user->identity->role == User::ROLE_ADMIN || Yii::$app->user->identity->role ==
        User::ROLE_MODERATOR
    ): ?>
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <? endif; ?>
    <div class="panel">
        <div class="panel-heading">
            <div class="">
                <div class="row">
                    <div class="col-sm-9">
                        <h3 class=""><?= $model->title ?></h3>
                        <h4 class="">
                            <small><em><?= date('d.m.Y', $model->created_at) ?>
                                    <?= date('H:i', $model->created_at) ?></em></small>
                        </h4>
                    </div>
                    <div class="col-sm-3">

                    </div>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <?= $model->body ?>
        </div>

        <div class="panel-footer">
            <span class="label label-default">Welcome</span> <span
                class="label label-default">Updates</span> <span class="label label-default">July</span>
        </div>
    </div>
    <? /*= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'author_id',
            'title',
            'body:ntext',
            'created_at',
            'updated_at',
        ],
    ])*/ ?>

</div>
