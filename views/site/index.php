<?php
use yii\helpers\Url;
use \yii\widgets\LinkPager;

/* @var $this yii\web\View */

$this->title = 'News portal';
?>

<div class="row">

    <div class="col-md-11">
        <div class="col-xs-2 pull-right">
            <?if(count($articles)):?>
            <select class="form-control" id="sel1" onchange="window.location='?page_count='+this.value">
                <? foreach ($pages as $page): ?>
                    <option <?= $pagesize == $page ? 'selected' : '' ?>><?= $page ?></option>
                <? endforeach ?>
            </select>
            <small>News count per-page</small>
            <?endif;?>
        </div>
        <div id="postlist">
            <? foreach ($articles as $article): ?>
                <div class="panel">
                    <div class="panel-heading">
                        <div class="">
                            <div class="row">
                                <div class="col-sm-9">
                                    <h3 class=""><?= $article->title ?></h3>

                                    <small><em><?= date('d.m.Y', $article->created_at) ?>
                                            <?= date('H:i', $article->created_at) ?></em></small>

                                </div>
                                <div class="col-sm-3">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                         <a href="<?= Url::to(['post/view', 'id' => $article->id]); ?>">Read
                            more</a>
                    </div>


                </div>
            <? endforeach ?>
            <?
            // display pagination
            echo LinkPager::widget([
                'pagination' => $pagination,
            ]);
            ?>

        </div>

    </div>

</div>

