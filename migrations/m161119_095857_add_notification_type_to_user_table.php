<?php

use yii\db\Migration;

class m161119_095857_add_notification_type_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'notification_type', "ENUM('email', 'browser', 'both')");
    }

    public function down()
    {
        $this->dropColumn('user', 'notification_type');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
