<?php
/**
 * Created by PhpStorm.
 * User: Sarvan Ibishov
 * Date: 11/19/2016
 * Time: 4:17 PM
 */

namespace app\controllers;

use Yii;
use app\models\User;
use yii\web\Controller;

class NotificationController extends Controller
{

    public function actionIndex()
    {

        $model = User::findOne(\Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post())) {
            $model->notification_type = Yii::$app->request->post()['User']['notification_type'];
            if ($model->save()) {

                return $this->refresh();
            }

        } else {
            return $this->render('../user/settings/notification', [
                'model' => $model,
            ]);
        }

    }
}