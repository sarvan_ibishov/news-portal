<?php
/**
 * Created by PhpStorm.
 * User: Sarvan Ibishov
 * Date: 11/17/2016
 * Time: 9:21 PM
 */

namespace app\controllers;

use app\components\NotificationQueue;
use app\models\User;
use dektrium\user\controllers\RegistrationController as BaseRegistrationController;
use dektrium\user\models\RegistrationForm;
use yii\web\NotFoundHttpException;

class RegistrationController extends BaseRegistrationController
{
    const EVENT_NEW_USER = 'new_user';

    public function sendNotificationToAdmin($model)
    {

        NotificationQueue::addQueue($this->finder->findUserByUsername($model->username)->id, $model->username, true);
    }

    public function actionConfirm($id, $code)
    {
        $user = $this->finder->findUserById($id);

        if ($user === null || $this->module->enableConfirmation == false) {
            throw new NotFoundHttpException();
        }

        $event = $this->getUserEvent($user);

        $this->trigger(self::EVENT_BEFORE_CONFIRM, $event);

        $user->attemptConfirmation($code);

        $this->trigger(self::EVENT_AFTER_CONFIRM, $event);
        $user->role = User::ROLE_USER;
        $user->update();
        return $this->render('/message', [
            'title' => \Yii::t('user', 'Account confirmation'),
            'module' => $this->module,
        ]);
    }

    public function actionRegister()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }
      //  $this->on(self::EVENT_NEW_USER, [$this, 'sendNotificationToAdmin']);
        /** @var RegistrationForm $model */
        $model = \Yii::createObject(RegistrationForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->request->post()) && $model->register()) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);
           // $this->trigger(self::EVENT_NEW_USER,$event);
            $this->sendNotificationToAdmin($model);
            return $this->render('/message', [
                'title' => \Yii::t('user', 'Your account has been created'),
                'module' => $this->module,
            ]);
        }

        return $this->render('register', [
            'model' => $model,
            'module' => $this->module,
        ]);
    }
}