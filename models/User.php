<?php

namespace app\models;

use dektrium\user\models\User as BaseUser;
use yii\helpers\ArrayHelper;

class User extends BaseUser
{
    /** public function register()
     * {
     * // do your magic
     * }*/
    const ROLE_USER = 10;
    const ROLE_MODERATOR = 20;
    const ROLE_ADMIN = 30;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        return ArrayHelper::merge($scenarios, [
            'register' => ['username', 'email', 'password'],
            'connect' => ['username', 'email'],
            'create' => ['username', 'email', 'password', 'role'],
            'update' => ['username', 'email', 'password', 'role'],
            'settings' => ['username', 'email', 'password'],
            'notification' => ['notification_type']

        ]);
    }
}

